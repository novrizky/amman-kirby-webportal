

// Collapse accordion every time dropdown is shown
// $('.dropdown-accordion').on('show.bs.dropdown', function (event) {
//   var accordion = $(this).find($(this).data('accordion'));
//   accordion.find('.panel-collapse.in').collapse('hide');
// });

// Prevent dropdown to be closed when we click on an accordion link
// $('.dropdown-accordion').on('click', 'a[data-toggle="collapse"]', function (event) {
//   event.preventDefault();
//   event.stopPropagation();
//   $($(this).data('parent')).find('.panel-collapse.in').collapse('hide');
//   $($(this).attr('href')).collapse('show');
// })


// footer contact action
function changeContact(param) {
  // console.log('asd');
  var address = document.getElementById('footerAddress');
  var contact = document.getElementById('footerContact');
  var bingMap = document.getElementById('bingMap');
  var bingMapLink = document.getElementById('bingMapLink');
  if(param==='jakarta'){
    address.innerHTML = "The Energy Building, 28th Floor SCBD Lot 11A<br/>Jl. Jenderal Sudirman Kav. 52-53<br/>Jakarta 12190 – Indonesia";
    contact.innerHTML = "Tlp : +61 21 5799 4600 (hunting)";
    bingMap.setAttribute("src","https://www.bing.com/maps/embed?h=180&w=320&cp=-6.225351014592164~106.80870280707666&lvl=15&typ=d&sty=r&src=SHELL&FORM=MBEDV8");
    bingMapLink.setAttribute("href", "https://www.bing.com/maps?osid=7f6d288f-d89b-4aaa-ad57-bb46ec52292f&cp=-6.225004~106.800907&lvl=16&v=2&sV=2&form=S00027");
  }else if(param==='mataram'){
    address.innerHTML = "Jl. Bung Karno no.6, Mataram<br/>Nusa Tenggara Barat – Indonesia";
    contact.innerHTML = "Tlp : +62 370 636318";
    bingMap.setAttribute("src","https://www.bing.com/maps/embed?h=180&w=320&cp=-8.590808696464237~116.11519788012463&lvl=15&typ=d&sty=r&src=SHELL&FORM=MBEDV8");
    bingMapLink.setAttribute("href", "https://www.bing.com/maps?&cp=-8.589453~116.110538&lvl=16&osid=1bad8c11-fb3e-438e-a60d-d8ec74544958&v=2&sV=2&form=S00027");
  }else if(param==='batuhijau'){
    address.innerHTML = "Townsite Buin Batu, Desa Sekongkang Atas<br/> Kecamatan Sekongkang, Sumbawa Barat<br/> Nusa Tenggara Barat – Indonesia";
    contact.innerHTML = "Tlp : +62 372 635318";
    bingMap.setAttribute("src","https://www.bing.com/maps/embed?h=180&w=320&cp=-8.905493464674933~116.75145046383406&lvl=15&typ=d&sty=r&src=SHELL&FORM=MBEDV8");
    bingMapLink.setAttribute("href", "https://www.bing.com/maps?&cp=-8.905494~116.74625&lvl=16&osid=d0531f66-dc4e-4ff9-908b-4039ab30c751&v=2&sV=2&form=S00027");
    
  }
}

// Load Calendar
document.addEventListener('DOMContentLoaded', function () {
  var initialLocaleCode = 'id';
  var calendarEl = document.getElementById('calendar');
  if (calendarEl) {
    var calendar = new FullCalendar.Calendar(calendarEl, {
      headerToolbar: {
        left: 'title',
        center: '',
        right: 'prev next'
      },
      locale: initialLocaleCode,
      columnFormat: 'd',
      events: [
        {
          start: '2021-02-20',
          end: '2021-02-20',
          overlap: false,
          display: 'background',
          color: '#E44C4C'
        },
        {
          start: '2021-02-21',
          end: '2021-02-21',
          overlap: false,
          display: 'background',
          color: '#0E9E88'
        },
      ],
    });
    calendar.render();
  }
});

$(document).ready(function () {

  // Department and Teams Navigation
  $('body').on('click', function (e) {
    if (!$('a#dropdownNavButton,  a#dropdownNavButton span, #accordionMenuLeft, #accordionMenuRight, li.nav-item a.nav-link.nav-dropdown-header, div.submenu-column.dnt-left, div.submenu-column.dnt-right, .dnt h4.panel-title a, .dnt h4.panel-title a i').is(e.target)
    ) {
      $('li.nav-item div.dropdown-menu.dnt').removeClass('show');
    }
  });

  $('li.nav-item a.nav-link.nav-dropdown-header.dnt-header').on('click', function (e) {

    if ($('li.nav-item div.dropdown-menu.dnt').hasClass('show')) {
      $('li.nav-item div.dropdown-menu.dnt').removeClass('show');
    } else {
      $('li.nav-item div.dropdown-menu.dnt').addClass('show');
    }

  });

  $('.dropdown-menu.dnt .panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.dropdown-menu.dnt .panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });

  // Load & Set Home Bottom Carousel
  $("#myCarousel1").on("slide.bs.carousel", function (e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $(".carousel-item").length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
      var it = itemsPerSlide - (totalItems - idx);
      for (var i = 0; i < it; i++) {
        // append slides to end
        if (e.direction == "left") {
          $(".carousel-item")
            .eq(i)
            .appendTo(".carousel-inner");
        } else {
          $(".carousel-item")
            .eq(0)
            .appendTo($(this).find(".carousel-inner"));
        }
      }
    }
  });

  $("#myCarousel2").on("slide.bs.carousel", function (e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $(".carousel-item").length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
      var it = itemsPerSlide - (totalItems - idx);
      for (var i = 0; i < it; i++) {
        // append slides to end
        if (e.direction == "left") {
          $(".carousel-item")
            .eq(i)
            .appendTo(".carousel-inner");
        } else {
          $(".carousel-item")
            .eq(0)
            .appendTo($(this).find(".carousel-inner"));
        }
      }
    }
  });

  $("#myCarousel3").on("slide.bs.carousel", function (e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $(".carousel-item").length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
      var it = itemsPerSlide - (totalItems - idx);
      for (var i = 0; i < it; i++) {
        // append slides to end
        if (e.direction == "left") {
          $(".carousel-item")
            .eq(i)
            .appendTo(".carousel-inner");
        } else {
          $(".carousel-item")
            .eq(0)
            .appendTo($(this).find(".carousel-inner"));
        }
      }
    }
  });

});
