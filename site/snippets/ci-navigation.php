<div class="col-12 ci-navbar">
    <a href="<?= $site->url() ?>">
        <?= svg('assets/icons/arrow-back.svg') ?>
    </a>
</div>
<div class="col-12 ci-navbar active">
    <a href="<?= $site->url() . '/company-information-boc' ?>">
        Board of Commissioner
    </a>
</div>
<div class="col-12 ci-navbar">
    <a href="<?= $site->url() . '/company-information-bod' ?>">
        Board of Director
    </a>
</div>
<div class="col-12 ci-navbar">
    <a href="<?= $site->url() . '/company-information-soc' ?>">
        Statement of Commitment
    </a>
</div>
<div class="col-12 ci-navbar">
    <a href="<?= $site->url() . '/company-information-orgchart' ?>">
        Organization Structure
    </a>
</div>