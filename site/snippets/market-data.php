<div class="col-12 market-data nopad">
    <div class="col market-data-header">
        <span>Market Data (US$)</span>
    </div>
    <table class="table table-striped market-data-table">
        <tbody>
            <tr>
                <td>LBMA Gold ($/oz)</td>
                <td>1871.05</td>
                <td class="market-data-percent percent-minus">-4.15</td>
            </tr>
            <tr>
                <td>CME&TR Silver ($/oz)</td>
                <td>1871.05</td>
                <td class="market-data-percent percent-minus">-4.15</td>
            </tr>
            <tr>
                <td>LME Copper ($/lb)</td>
                <td>1871.05</td>
                <td class="market-data-percent percent-plus">+4.15</td>
            </tr>
        </tbody>
    </table>
    <div class="col market-data-footer">
        Last Update : 2020-12-23
    </div>
</div>