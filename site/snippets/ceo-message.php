<div class="col-12 ceo-message">
    <div class="row">
        <div class="col ceo-message-content vertical-center" style="color:#fff">
            <p>The harder you work for something, the greater you'll feel when you achieve it.</p>
            <hr/>
        </div>
    </div>
</div>