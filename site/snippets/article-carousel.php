<div class="col-12 article-carousel">
    <div class="col-12 article-carousel-header"><?= $title ?></div>
    <div class="col-12 article-carousel-body nopad">
        <div class="container-fluid nopad">
            <div id="<?= $carousel_id ?>" class="carousel" data-interval="false" data-ride="carousel">
                <div class="carousel-inner row w-100 mx-auto">
                    <div class="carousel-item col-md-4 active">
                        <div class="card">
                            <img class="card-img-top img-fluid" src="http://placehold.it/800x600/f44242/fff" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">Card 1</h4>
                                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <div class="col text-center card-button">
                                    <button class="btn btn-default">Read More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item col-md-4">
                        <div class="card">
                            <img class="card-img-top img-fluid" src="http://placehold.it/800x600/418cf4/fff" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">Card 2</h4>
                                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <div class="col text-center card-button">
                                    <button class="btn btn-default">Read More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item col-md-4">
                        <div class="card">
                            <img class="card-img-top img-fluid" src="http://placehold.it/800x600/3ed846/fff" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">Card 3</h4>
                                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <div class="col text-center card-button">
                                    <button class="btn btn-default">Read More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item col-md-4">
                        <div class="card">
                            <img class="card-img-top img-fluid" src="http://placehold.it/800x600/42ebf4/fff" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">Card 4</h4>
                                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <div class="col text-center card-button">
                                    <button class="btn btn-default">Read More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#<?= $carousel_id ?>" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#<?= $carousel_id ?>" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>