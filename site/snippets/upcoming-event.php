<div class="col-12 upcoming-event nopad">
    <div class="col-12 upcoming-event-header">
        <span>Upcoming Event</span>
    </div>

    <div class="col-12 upcoming-event-body">
        <div class="row">
            <div class="col-6 upcoming-event-left">
                <div id="calendar"></div>
            </div>
            <div class="col-6 upcoming-event-right scrollable">
                <div class="col upcoming-event-list" data-toggle="modal" data-target="#announcementModal">
                    <div class="row">
                        <div class="col-3 upcoming-event-card green">
                            <p>13</p>
                            <p>Oct</p>
                        </div>
                        <div class="col-9 upcoming-event-detail">
                            <h4>Collab 365 Summit</h4>
                            <p>Thu, 22 Jan | 9 - 10 PM</p>
                        </div>
                    </div>
                </div>
                <div class="col upcoming-event-list" data-toggle="modal" data-target="#announcementModal">
                    <div class="row">
                        <div class="col-3 upcoming-event-card red">
                            <p>13</p>
                            <p>Oct</p>
                        </div>
                        <div class="col-9 upcoming-event-detail">
                            <h4>Collab 365 Summit</h4>
                            <p>Thu, 22 Jan | 9 - 10 PM</p>
                        </div>
                    </div>
                </div>
                <div class="col upcoming-event-list" data-toggle="modal" data-target="#announcementModal">
                    <div class="row">
                        <div class="col-3 upcoming-event-card green">
                            <p>13</p>
                            <p>Oct</p>
                        </div>
                        <div class="col-9 upcoming-event-detail">
                            <h4>Collab 365 Summit</h4>
                            <p>Thu, 22 Jan | 9 - 10 PM</p>
                        </div>
                    </div>
                </div>
                <div class="col upcoming-event-list" data-toggle="modal" data-target="#announcementModal">
                    <div class="row">
                        <div class="col-3 upcoming-event-card green">
                            <p>13</p>
                            <p>Oct</p>
                        </div>
                        <div class="col-9 upcoming-event-detail">
                            <h4>Collab 365 Summit</h4>
                            <p>Thu, 22 Jan | 9 - 10 PM</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->

<div class="modal fade bd-example-modal-lg" id="announcementModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-upcoming-event" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4 class="event-title">Event Title</h4>
                <p class="event-description">
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                </p>
                <p class="event-time">
                    Thurday, 22 Jan 9 - 10 AM
                </p>
                <span>Venue</span>
                <p class="event-venue">
                    Four Season Hotel
                    Jl. Gatot Subroto No.Kav. 18, Kuningan, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12710•(021) 22771888
                </p>
                <span>Open Registration</span>
                <a href="https://www.google.com">
                    <p class="event-link">
                        https://bit.ly.registrasi
                    </p>
                </a>
                <span>Contact Person</span>
                <p class="event-contact">
                    Dhani +6281210601873
                </p>
            </div>

        </div>
    </div>
</div>