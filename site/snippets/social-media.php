<div class="col-12 social-media nopad">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" href="#1" data-toggle="tab"><?= svg('assets/icons/twitter.svg') ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#2" data-toggle="tab"><?= svg('assets/icons/discord.svg') ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#3" data-toggle="tab"><?= svg('assets/icons/instagram.svg') ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#4" data-toggle="tab"><?= svg('assets/icons/twitter.svg') ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#5" data-toggle="tab"><?= svg('assets/icons/discord.svg') ?></a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="1">
            <h3>Standard tab panel created on bootstrap using nav-tabs</h3>
        </div>
        <div class="tab-pane" id="2">
            <h3>Notice the gap between the content and tab after applying a background color</h3>
        </div>
        <div class="tab-pane" id="3">
            <h3>add clearfix to tab-content (see the css)</h3>
        </div>
        <div class="tab-pane" id="4">
            <h3>Notice the gap between the content and tab after applying a background color</h3>
        </div>
        <div class="tab-pane" id="5">
            <h3>add clearfix to tab-content (see the css)</h3>
        </div>
    </div>
</div>