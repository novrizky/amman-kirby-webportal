<div class="col-12 pending-approval nopad">
    <div id="accordion">
        <div class="card">
            <div class="card-header collapsed" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                <h5 class="mb-0">
                    <a class="btn btn-link collapsed">
                        My Pending Approval
                        <i></i>
                    </a>

                </h5>
            </div>

            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body pending-approval-card">
                    <div class="pending-approval-app">
                        <a data-toggle="collapse" href="#AccordionAppsName1" role="button" aria-expanded="false" aria-controls="collapseExample">
                            <p>
                                Ellips (2)
                            </p>
                        </a>
                        <div class="collapse" id="AccordionAppsName1">
                            <table class="table pending-approval-table">
                                <tbody>
                                    <tr>
                                        <td><a href="#">1234</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="#">1234</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="pending-approval-app">
                        <a data-toggle="collapse" href="#AccordionAppsName2" role="button" aria-expanded="false" aria-controls="collapseExample">
                            <p>
                                ISS (1)
                            </p>
                        </a>
                        <div class="collapse" id="AccordionAppsName2">
                            <table class="table pending-approval-table">
                                <tbody>
                                    <tr>
                                        <td><a href="#">1234</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>