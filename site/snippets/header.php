<?php
/*
  Snippets are a great way to store code snippets for reuse
  or to keep your templates clean.

  This header snippet is reused in all templates.
  It fetches information from the `site.txt` content file
  and contains the site navigation.

  More about snippets:
  https://getkirby.com/docs/guide/templates/snippets
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <?php
  /*
    In the title tag we show the title of our
    site and the title of the current page
  */
  ?>
  <title><?= $site->title() ?> | <?= $page->title() ?></title>

  <?php
  /*
    Stylesheets can be included using the `css()` helper.
    Kirby also provides the `js()` helper to include script file.
    More Kirby helpers: https://getkirby.com/docs/reference/templates/helpers
  */
  ?>
  <?= css([
    'assets/css/prism.css',
    'assets/css/lightbox.css',
    'assets/css/index.css',
    '@auto'
  ]) ?>

  <?php
  /*
    The `url()` helper is a great way to create reliable
    absolute URLs in Kirby that always start with the
    base URL of your site.
  */
  ?>
  <link rel="shortcut icon" type="image/x-icon" href="<?= url('favicon.ico') ?>">
  <?= css([
    'assets/css/bootstrap.min.css',
    'assets/lib/fullcalendar/main.min.css',
    'assets/css/fullcalendar-custom.css',
    'assets/css/custom.css',
  ]) ?>
</head>

<body>

  <?php
  include_once 'navigation.php';
  ?>

  <nav class="navbar navbar-expand-lg navbar-light">
    <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button> -->
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
      <a class="navbar-brand" href="<?= $site->url() ?>">
        <img src="<?= url('assets/icons/logo.svg') ?>" class="d-inline-block align-top company-logo" alt="">
      </a>
      <p class="portal-brand-name">
        <a href="<?= $site->url() ?>">
          Prospector
        </a>
      </p>
      <ul class="navbar-nav ml-auto mr-auto mt-2 mt-lg-0">
        <li class="nav-item active">
          <a class="nav-link nav-dropdown-header dnt-header" id="dropdownNavButton" aria-haspopup="true" aria-expanded="false" href="#"><span>Department and Teams</span></a>
          <div class="dropdown-menu dnt" aria-labelledby="dropdownNavButton">

            <div class="row submenu-container">

              <!-- Department and Teams - Left -->

              <div class="col-sm submenu-column dnt-left">
                <div class="wrapper center-block">
                  <div class="panel-group" id="accordionMenuLeft" role="tablist" aria-multiselectable="true">
                    <?php
                    $idx = 1;
                    foreach ($dnt_nav_left as $menu) {
                      if ($menu['child']) {
                    ?>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="headingLeftNav<?= $idx ?>">
                            <h4 class="panel-title">
                              <a role="button" data-toggle="collapse" data-parent="#accordionMenuLeft" href="#collapseLeftNav<?= $idx ?>" aria-expanded="true" aria-controls="collapseLeftNav<?= $idx ?>">
                                <?= $menu['title'] ?> <i></i>
                              </a>
                            </h4>
                          </div>
                          <div id="collapseLeftNav<?= $idx ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingLeftNav<?= $idx ?>">
                            <div class="panel-body">
                              <?php
                              foreach ($menu['child'] as $submenu) {
                              ?>
                                <a href="<?= $submenu['url'] ?>">
                                  <p class="navigation-link child"><?= $submenu['title'] ?></p>
                                </a>
                              <?php
                              }
                              ?>
                            </div>
                          </div>
                        </div>
                      <?php
                      } else {
                      ?>
                        <a href="<?= $menu['url'] ?>">
                          <p class="navigation-link"><?= $menu['title'] ?></p>
                        </a>
                      <?php
                      }
                      ?>

                    <?php
                      $idx++;
                    }
                    ?>

                  </div>
                </div>
              </div>

              <!-- Department and Teams - Right -->

              <div class="col-sm submenu-column dnt-right">
                <div class="wrapper center-block">
                  <div class="panel-group" id="accordionMenuRight" role="tablist" aria-multiselectable="true">
                    <?php
                    $idx = 1;
                    foreach ($dnt_nav_right as $menu) {
                      if ($menu['child']) {
                    ?>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="headingRightNav<?= $idx ?>">
                            <h4 class="panel-title">
                              <a role="button" data-toggle="collapse" data-parent="#accordionMenuRight" href="#collapseRightNav<?= $idx ?>" aria-expanded="true" aria-controls="collapseRightNav<?= $idx ?>">
                                <?= $menu['title'] ?> <i></i>
                              </a>
                            </h4>
                          </div>
                          <div id="collapseRightNav<?= $idx ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingRightNav<?= $idx ?>">
                            <div class="panel-body">
                              <?php
                              foreach ($menu['child'] as $submenu) {
                              ?>
                                <a href="<?= $submenu['url'] ?>">
                                  <p class="navigation-link child"><?= $submenu['title'] ?></p>
                                </a>
                              <?php
                              }
                              ?>
                            </div>
                          </div>
                        </div>
                      <?php
                      } else {
                      ?>
                        <a href="<?= $menu['url'] ?>">
                          <p class="navigation-link"><?= $menu['title'] ?></p>
                        </a>
                      <?php
                      }
                      ?>

                    <?php
                      $idx++;
                    }
                    ?>

                  </div>
                </div>
              </div>
            </div>
          </div>

        </li>
        <li class="nav-item">
          <a class="nav-link  nav-dropdown-header" href="<?= $site->url() . '/employee-resources' ?>">Employee Resources</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  nav-dropdown-header" href="<?= $site->url() . '/company-information-boc' ?>">Company Information</a>
        </li>

      </ul>

      <ul class="navbar-nav ml-auto mt-2 mt-lg-0 navbar-nav-right">
        <li class="nav-item">
          <a class="nav-link nav-dropdown-header" href="#">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link dropdown-toggle cc" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Call Center
          </a>
          <div class="dropdown-menu cc" aria-labelledby="navbarDropdownMenuLink">
            <div class="row submenu-container">
              <div class="col-sm submenu-column cc-column">
                <?php
                foreach ($cc_list[0] as $cc) {
                ?>
                  <div class="row">
                    <div class="col-8 cc-title">
                      <?= $cc['title'] ?>
                    </div>
                    <div class="col-2 cc-icon">
                      <?php
                      if ($cc['email']) {
                        echo "<a href='" . $cc['email'] . "'>" . svg("assets/icons/call-center-email.svg") . "</a>";
                      }
                      ?>
                    </div>
                    <div class="col-2 cc-icon">
                      <?php
                      if ($cc['phone']) {
                        echo "<a href='" . $cc['phone'] . "'>" . svg("assets/icons/call-center-phone.svg") . "</a>";
                      }
                      ?>
                    </div>
                  </div>
                <?php
                }
                ?>
              </div>
              <div class="col-sm submenu-column cc-column">
                <?php
                foreach ($cc_list[1] as $cc) {
                ?>
                  <div class="row">
                    <div class="col-8 cc-title">
                      <?= $cc['title'] ?>
                    </div>
                    <div class="col-2 cc-icon">
                      <?php
                      if ($cc['email']) {
                        echo "<a href='" . $cc['email'] . "'>" . svg("assets/icons/call-center-email.svg") . "</a>";
                      }
                      ?>
                    </div>
                    <div class="col-2 cc-icon">
                      <?php
                      if ($cc['phone']) {
                        echo "<a href='" . $cc['phone'] . "'>" . svg("assets/icons/call-center-phone.svg") . "</a>";
                      }
                      ?>
                    </div>
                  </div>
                <?php
                }
                ?>
              </div>
            </div>
            <!-- <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a> -->
          </div>
        </li>
      </ul>
      <!-- <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form> -->
    </div>
  </nav>

  <main class="main">