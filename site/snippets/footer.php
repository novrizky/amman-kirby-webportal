<?php
/*
  Snippets are a great way to store code snippets for reuse
  or to keep your templates clean.

  This footer snippet is reused in all templates.

  More about snippets:
  https://getkirby.com/docs/guide/templates/snippets
*/
?>
</main>

<footer class="footer">
  <div class="row">
    <div class="col-2 footer-left">
      <img src="<?= url('assets/icons/footer-logo.svg') ?>" class="img-fluid" alt="">
    </div>
    <div class="col-6 footer-mid">
      <div class="row">
        <div class="col-4 footer-mid-left">
          <h3 class="footer-title">
            Contact
          </h3>
          <ul>
            <li onclick="changeContact('jakarta')">
              Kantor Jakarta
            </li>
            <li onclick="changeContact('mataram')">
              Kantor Cabang Mataram
            </li>
            <li onclick="changeContact('batuhijau')">
              Site Batu Hijau
            </li>
          </ul>
        </div>
        <div class="col-8 footer-mid-right">
          <h3 class="footer-title">
            Address
          </h3>
          <p class="footer-address" id="footerAddress">
            The Energy Building, 28th Floor SCBD Lot 11A<br />Jl. Jenderal Sudirman Kav. 52-53<br />Jakarta 12190 – Indonesia
          </p>
          <p class="footer-contact" id="footerContact">
            Tlp : +61 21 5799 4600 (hunting)
          </p>
        </div>
      </div>
    </div>
    <div class="col-4 footer-right">
      <a id="bingMapLink" href="https://www.bing.com/maps?osid=7f6d288f-d89b-4aaa-ad57-bb46ec52292f&cp=-6.225004~106.800907&lvl=16&v=2&sV=2&form=S00027" target="_blank">
        <div class="map-link"></div>
      </a>
      <div>

        <iframe id="bingMap" width="320" height="180" frameborder="0" src="https://www.bing.com/maps/embed?h=180&w=320&cp=-6.225351014592164~106.80870280707666&lvl=15&typ=d&sty=r&src=SHELL&FORM=MBEDV8" scrolling="no">
        </iframe>

      </div>
    </div>
  </div>
  <!-- <div class="grid">
      <div class="column" style="--columns: 8">
        <h2><a href="https://getkirby.com">Made with Kirby</a></h2>
        <p>
          Kirby: the file-based CMS that adapts to any project, loved by developers and editors alike
        </p>
      </div>
      <div class="column" style="--columns: 2">
        <h2>Pages</h2>
        <ul>
          <?php foreach ($site->children()->listed() as $example) : ?>
          <li><a href="<?= $example->url() ?>"><?= $example->title()->html() ?></a></li>
          <?php endforeach ?>
        </ul>
      </div>
      <div class="column" style="--columns: 2">
        <h2>Kirby</h2>
        <ul>
          <li><a href="https://getkirby.com">Website</a></li>
          <li><a href="https://getkirby.com/docs">Docs</a></li>
          <li><a href="https://forum.getkirby.com">Forum</a></li>
          <li><a href="https://chat.getkirby.com">Chat</a></li>
          <li><a href="https://github.com/getkirby">GitHub</a></li>
        </ul>
      </div>
    </div> -->
</footer>

<?= js([
  'assets/js/prism.js',
  'assets/js/lightbox.js',
  'assets/js/index.js',
  '@auto',
  'assets/js/jquery.min.js',
  'assets/js/bootstrap.min.js',
  'assets/lib/fullcalendar/main.min.js',
  'assets/js/custom.js',
]) ?>

</body>

</html>