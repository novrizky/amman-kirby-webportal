<div class="ci-dnc-detail">
    <div class="col-12 nopad dnc-detail-top">
        <div class="row">
            <div class="col-4 dnc-detail-picture">
                <img src="http://placehold.it/225x300" class="img-responsive img-rounded" alt="">
            </div>
            <div class="col-8 dnc-detail-data">
                <p class="dnc-detail-name">Rachmat Makkasau</p>
                <p class="dnc-detail-title">Presiden Director</p>
                <p class="dnc-detail-description">Rachmat Makkasau telah bekerja di tambang Batu Hijau sejak 1999, pada masa persiapan operasi Batu Hijau. Beliau memiliki pengalaman lebih dari 21 tahun di industri pertambangan. Selama 16 tahun terakhir ini, beliau telah menjadi bagian dari keberhasilan perusahaan dengan kepemimpinanya di berbagai bidang penting bisnis perusahaan seperti di bidang Pengelolahan, Penambangan, Kesehatan & Keselamatan Kerja dan terakhir memimpin bidang Sustainability & External Relations.</p>
            </div>
        </div>
    </div>
    <div class="col-12 nopad dnc-detail-bottom">
    </div>
</div>