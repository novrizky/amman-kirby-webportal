<div class="col-12 announcement nopad">
    <div class="col-12 announcement-header">
        <span>Announcement</span>
    </div>
    <div class="col-12 announcement-body scrollable">
        <div class="announcement-list">
            <div class="row">
                <div class="col-4">
                    <img src="https://via.placeholder.com/1024x768.png" class="rounded mx-auto d-block" alt="...">
                </div>
                <div class="col-8">
                    <p class="announcement-list-title">Announcement List Title</p>
                    <p class="announcement-list-content">Lorem ipsum dolor sit amet, conscetur adipiscing elit.. <a href="">read more</a></p>
                    <span class="announcement-list-date">Kamis, 4 Februari 2021</span>
                </div>
            </div>
        </div>
        <div class="announcement-list">
            <div class="row">
                <div class="col-4">
                    <img src="https://via.placeholder.com/1024x768.png" class="rounded mx-auto d-block" alt="...">
                </div>
                <div class="col-8">
                    <p class="announcement-list-title">Announcement List Title</p>
                    <p class="announcement-list-content">Lorem ipsum dolor sit amet, conscetur adipiscing elit.. <a href="">read more</a></p>
                    <span class="announcement-list-date">Kamis, 4 Februari 2021</span>
                </div>
            </div>
        </div>
        <div class="announcement-list">
            <div class="row">
                <div class="col-4">
                    <img src="https://via.placeholder.com/1024x768.png" class="rounded mx-auto d-block" alt="...">
                </div>
                <div class="col-8">
                    <p class="announcement-list-title">Announcement List Title</p>
                    <p class="announcement-list-content">Lorem ipsum dolor sit amet, conscetur adipiscing elit.. <a href="">read more</a></p>
                    <span class="announcement-list-date">Kamis, 4 Februari 2021</span>
                </div>
            </div>
        </div>
    </div>
</div>

