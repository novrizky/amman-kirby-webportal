<?php
/*
  Snippets are a great way to store code snippets for reuse
  or to keep your templates clean.

  This footer snippet is reused in all templates.

  More about snippets:
  https://getkirby.com/docs/guide/templates/snippets
*/
?>
</main>

<?= js([
  'assets/js/prism.js',
  'assets/js/lightbox.js',
  'assets/js/index.js',
  '@auto',
  'assets/js/jquery.min.js',
  'assets/js/bootstrap.min.js',
  'assets/lib/fullcalendar/main.min.js',
  'assets/js/custom.js',
]) ?>

</body>

</html>