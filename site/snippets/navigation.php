<?php
$dnt_nav_left = array(
    array(
        "title" => "Mining",
        "url" => null,
        "child" => array(
            array(
                "title" => "Engineering",
                "url" => null,
            ),
            array(
                "title" => "Geology",
                "url" => null,
            ),
            array(
                "title" => "Maintenance",
                "url" => null,
            ),
            array(
                "title" => "Operations",
                "url" => null,
            ),
            array(
                "title" => "Watermanagement & Project",
                "url" => null,
            )
        )
    ),
    array(
        "title" => "Process",
        "url" => null,
        "child" => array(
            array(
                "title" => "Operations",
                "url" => null,
            ),
            array(
                "title" => "Maintenance",
                "url" => null,
            ),
            array(
                "title" => "Power Plant",
                "url" => null,
            )
        )
    ),
    array(
        "title" => "Operations Support",
        "url" => null,
        "child" => array(
            array(
                "title" => "Business Process",
                "url" => null,
            ),
            array(
                "title" => "Facilities and Services",
                "url" => null,
            ),
            array(
                "title" => "Fleet",
                "url" => null,
            ),
            array(
                "title" => "Port Operations",
                "url" => null,
            ),
            array(
                "title" => "SCM",
                "url" => null,
            ),
            array(
                "title" => "Transport",
                "url" => null,
            )
        )
    ),
    array(
        "title" => "Exploration",
        "url" => null,
        "child" => null,
    ),
    array(
        "title" => "Rapid Response Team",
        "url" => null,
        "child" => null,
    ),
    array(
        "title" => "Legal",
        "url" => null,
        "child" => null,
    ),
    array(
        "title" => "Marketing",
        "url" => null,
        "child" => null,
    ),
    array(
        "title" => "Corporate Communications",
        "url" => null,
        "child" => null,
    ),
    array(
        "title" => "Environtment",
        "url" => null,
        "child" => null,
    ),
    array(
        "title" => "Finance",
        "url" => null,
        "child" => array(
            array(
                "title" => "Account Payable",
                "url" => null,
            ),
            array(
                "title" => "Account Receivable",
                "url" => null,
            ),
            array(
                "title" => "Capital",
                "url" => null,
            ),
            array(
                "title" => "Costing",
                "url" => null,
            ),
            array(
                "title" => "Tax",
                "url" => null,
            ),
            array(
                "title" => "GL",
                "url" => null,
            ),
            array(
                "title" => "Sox",
                "url" => null,
            )
        )
    ),
);

$dnt_nav_right = array(
    array(
        "title" => "Operations Planning",
        "url" => null,
        "child" => array(
            array(
                "title" => "Integrated Management System",
                "url" => null,
            ),
            array(
                "title" => "Full Potential",
                "url" => null,
            ),
            array(
                "title" => "Investment System",
                "url" => null,
            )
        )
    ),
    array(
        "title" => "HSLP",
        "url" => null,
        "child" => array(
            array(
                "title" => "Industrila Hygiene",
                "url" => null,
            ),
            array(
                "title" => "Emergency Response Team",
                "url" => null,
            ),
        )
    ),
    array(
        "title" => "HR",
        "url" => null,
        "child" => array(
            array(
                "title" => "Talent Development",
                "url" => null,
            ),
            array(
                "title" => "Expatriate Affairs",
                "url" => null,
            ),
            array(
                "title" => "Employee & Industrial Relations",
                "url" => null,
            ),
            array(
                "title" => "Compensation & Benefit",
                "url" => null,
            ),
            array(
                "title" => "Recruitment",
                "url" => null,
            ),
            array(
                "title" => "HRIS & Manpower Planning",
                "url" => null,
            ),
        )
    ),
    array(
        "title" => "IT",
        "url" => null,
        "child" => array(
            array(
                "title" => "PMO",
                "url" => null,
            ),
            array(
                "title" => "Solution",
                "url" => null,
            ),
            array(
                "title" => "Service Delivery",
                "url" => null,
            )
        )
    ),
    array(
        "title" => "Project Development",
        "url" => null,
        "child" => array(
            array(
                "title" => "Engineering",
                "url" => null,
            ),
            array(
                "title" => "Construction",
                "url" => null,
            ),
            array(
                "title" => "Project Control",
                "url" => null,
            ),
        )
    ),
    array(
        "title" => "Security",
        "url" => null,
        "child" => null,
    ),
    array(
        "title" => "Social Responsibility & Goverment Relation",
        "url" => null,
        "child" => null,
    ),
    array(
        "title" => "Training",
        "url" => null,
        "child" => null,
    ),
    array(
        "title" => "Clinic",
        "url" => null,
        "child" => null,
    ),
);


$call_center_list = array(
    array(
        "title" => "Emergency Response Teams",
        "email" => null,
        "phone" => "tel:49999",
    ),
    array(
        "title" => "Security",
        "email" => "mailto:Emergency.Command.Center@amman.co.id",
        "phone" => "tel:48040",
    ),
    array(
        "title" => "Environment",
        "email" => null,
        "phone" => "tel:49444",
    ),
    array(
        "title" => "IT Service Desk",
        "email" => "mailto:IT.Helpdesk@amman.co.id",
        "phone" => "tel:48888",
    ),
    array(
        "title" => "Maintenance Helpdesk",
        "email" => "mailto:Maintenance.Helpdesk@amman.co.id",
        "phone" => "tel:48096",
    ),
    array(
        "title" => "Sea Plant Manifest",
        "email" => "mailto:Sea.Plane.Manifest@amman.co.id",
        "phone" => "tel:48844",
    ),
    array(
        "title" => "Boat Manifest",
        "email" => "mailto:Boat.Manifest@amman.co.id",
        "phone" => "tel:46999",
    ),
    array(
        "title" => "Bus Transporation",
        "email" => "mailto:Bus.Transport@amman.co.id",
        "phone" => "tel:48991",
    ),
    array(
        "title" => "Buin Batu Clinic",
        "email" => "mailto:Clinic.Townsite@amman.co.id",
        "phone" => "tel:48201",
    ),
    array(
        "title" => "Packmeal",
        "email" => "mailto:Packmeal@amman.co.id",
        "phone" => "tel:48847",
    ),
    array(
        "title" => "Camp Service",
        "email" => "mailto:Camp.Service@amman.co.id",
        "phone" => "tel:48414",
    ),
    array(
        "title" => "HR Recruitment",
        "email" => "mailto:recruitment@amman.co.id",
        "phone" => "tel:48733",
    ),
    array(
        "title" => "Bank BNI",
        "email" => null,
        "phone" => "tel:48837",
    ),
    array(
        "title" => "Bank Mandiri",
        "email" => null,
        "phone" => "tel:48077",
    ),
    array(
        "title" => "Mail RM Mataram",
        "email" => null,
        "phone" => "tel:43021",
    ),
    array(
        "title" => "Mail RM Townsite",
        "email" => "mailto:Mail.Room@amman.co.id",
        "phone" => "tel:48672",
    ),
    array(
        "title" => "Pandusiwi",
        "email" => null,
        "phone" => "tel:49250",
    ),
    array(
        "title" => "Pest Control",
        "email" => null,
        "phone" => "tel:48842",
    ),
);

$cc_list = array_chunk($call_center_list, ceil(count($call_center_list)/2));