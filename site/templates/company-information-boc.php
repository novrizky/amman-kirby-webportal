<?php
/*
  Templates render the content of your pages.

  They contain the markup together with some control structures
  like loops or if-statements. The `$page` variable always
  refers to the currently active page.

  To fetch the content from each field we call the field name as a
  method on the `$page` object, e.g. `$page->title()`.

  This home template renders content from others pages, the children of
  the `photography` page to display a nice gallery grid.

  Snippets like the header and footer contain markup used in
  multiple templates. They also help to keep templates clean.

  More about templates: https://getkirby.com/docs/guide/templates/basics
*/
?>
<?php snippet('header') ?>
<div class="ci-navigation">
    <?php snippet('ci-navigation') ?>
</div>
<div class="ci-content-wrapper">
    <div class="col-12 ci-content nopad">
        <div class="row">
            <div class="col-9">
                <?php snippet('ci-dnc-detail') ?>
            </div>
            <div class="col-3">
                <?php snippet('ci-dnc-list') ?>
            </div>
        </div>
        <!-- <div class="col-12 mors-page-title">
            <?php snippet('mors-page-title') ?>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-4 nopad">
                    <div class="col-12">
                        <?php snippet('mors-realtime-dashboard') ?>
                    </div>
                    <div class="col-12">
                        <?php snippet('mors-historical-dashboard') ?>
                    </div>
                </div>
                <div class="col-4 nopad">
                    <?php snippet('mors-static-report') ?>
                </div>
                <div class="col-4 nopad">
                    <div class="col-12">
                        <?php snippet('mors-xml-report') ?>
                    </div>
                    <div class="col-12">
                        <?php snippet('mors-external-link') ?>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</div>
<?php snippet('footer') ?>