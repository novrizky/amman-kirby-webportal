<?php
/*
  Templates render the content of your pages.

  They contain the markup together with some control structures
  like loops or if-statements. The `$page` variable always
  refers to the currently active page.

  To fetch the content from each field we call the field name as a
  method on the `$page` object, e.g. `$page->title()`.

  This home template renders content from others pages, the children of
  the `photography` page to display a nice gallery grid.

  Snippets like the header and footer contain markup used in
  multiple templates. They also help to keep templates clean.

  More about templates: https://getkirby.com/docs/guide/templates/basics
*/
?>
<?php snippet('header') ?>

<div class="container home">
  <div class="row">
    <div class="col-9 home-left nopad">
      <div class="col-12 sub-content">
        <?php snippet('banner') ?>
      </div>
      <div class="col-12">
        <div class="row">
          <div class="col-7 sub-content">
            <?php snippet('upcoming-event') ?>
          </div>
          <div class="col-5 sub-content">
            <?php snippet('announcement') ?>
          </div>
        </div>
      </div>
      <div class="col-12 sub-content">
        <?php snippet('ceo-message') ?>
      </div>
      <div class="col-12 sub-content">
        <?php snippet('article-carousel', ['title' => 'Operational Excellence', 'carousel_id' => 'myCarousel1']) ?>
      </div>
      <div class="col-12 sub-content">
        <?php snippet('article-carousel', ['title' => 'Creating Legacy of Best', 'carousel_id' => 'myCarousel2']) ?>
      </div>
      <div class="col-12 sub-content">
        <?php snippet('article-carousel', ['title' => 'Employee Wellbeing', 'carousel_id' => 'myCarousel3']) ?>
      </div>
    </div>
    <div class="col-3 home-right nopad">
      <div class="col-12 sub-content">
        <?php snippet('custom-link-top') ?>
      </div>
      <div class="col-12 sub-content">
        <?php snippet('market-data') ?>
      </div>
      <div class="col-12 sub-content">
        <?php snippet('social-media') ?>
      </div>
      <div class="col-12 sub-content">
        <?php snippet('custom-link-bottom') ?>
      </div>
      <div class="col-12 sub-content">
        <?php snippet('my-pending-approval') ?>
      </div>
    </div>
  </div>
</div>

<?php snippet('footer') ?>