<?php
/*
  Templates render the content of your pages.

  They contain the markup together with some control structures
  like loops or if-statements. The `$page` variable always
  refers to the currently active page.

  To fetch the content from each field we call the field name as a
  method on the `$page` object, e.g. `$page->title()`.

  This home template renders content from others pages, the children of
  the `photography` page to display a nice gallery grid.

  Snippets like the header and footer contain markup used in
  multiple templates. They also help to keep templates clean.

  More about templates: https://getkirby.com/docs/guide/templates/basics
*/
?>
<?php snippet('header') ?>

<div class="container employee-resources">
  <div class="row employee-resources-top">
    <div class="col-5">
      <div class="col-12 sub-content">
        <h3>Employee Resources</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      </div>
    </div>
    <div class="col-7  nopad">
      <img class="img-responsive" src="https://via.placeholder.com/1280x720.jpg" alt="First slide">
    </div>
  </div>
  <div class="row employee-resources-bottom">
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="link-card">
        <img src="https://via.placeholder.com/50x50.jpg" class="img-responsive rounded float-left" alt="Application Name">
        <div class="link-card-name">
          Application Name
        </div>
      </div>
    </div>
  </div>
</div>

<?php snippet('footer') ?>